from django import forms
from .models import Status

class CreateStatus(forms.ModelForm):
    class Meta:
        model = Status
        fields = {'message'}
        widgets = {
            'message': forms.TextInput(attrs = {'class': 'form-control'}),
        }

