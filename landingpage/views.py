from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Status
from .forms import CreateStatus

# Create your views here.
def landingpage(request):
    if request.method == 'POST':
    	form = CreateStatus(request.POST)
    	if form.is_valid():
    		form.save()
    		return redirect('landingpage')
    else:
    	form = CreateStatus()

    stats = Status.objects.all().order_by('-date')
    return render(request, 'landingpage.html', {'form':form, 'stats':stats})