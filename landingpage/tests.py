from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse

from .views import landingpage
from .models import Status
from .forms import CreateStatus

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class lab6UnitTest(TestCase):
    def test_lab6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_urls_is_not_exist(self):
        response = self.client.get('/')
        self.assertFalse(response.status_code==404)

    def test_lab6_using_landingpage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, landingpage)

    def test_lab6_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landingpage.html')

    def test_lab6_text_is_exist(self):
        response = Client().get('/')
        self.assertContains(response, "Halo, Apa kabar?")

    def test_lab6_form_validation_for_blank_items(self):
        form = CreateStatus(data={'message': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['message'],
            ["This field is required."]
        )
    def test_lab6_check_model(self):
        input = Status.objects.create(message='halo')
        counting_all_input = Status.objects.all().count()
        self.assertEqual(counting_all_input, 1)

    def test__lab6_models(self):
        data = Status(message = 'tes')
        data.save()
        response = Client().get('/')    
        self.assertContains(response, 'tes')

    def test_lab6_add_status(self):
        data = {"message" : "halo"}
        response = Client().post('/', data)
        self.assertEqual(response.status_code, 302)

    def test_lab6_status_form(self):
        data = {'message' : 'halo'}
        form = CreateStatus(data=data)
        self.assertTrue(form.is_valid())

class lab6FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(lab6FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.refresh()
        self.browser.quit()
        super(lab6FunctionalTest, self).tearDown()

    def test_input(self):
        self.browser.get(self.live_server_url)
        status = self.browser.find_element_by_id('id_message')
        status.send_keys("Coba coba")
        #time.sleep(3)
        submit = self.browser.find_element_by_id('submit')
        submit.send_keys(Keys.RETURN)
        #time.sleep(3)
        self.browser.get(self.live_server_url)
        #time.sleep(3)
        self.assertIn('Coba coba', self.browser.page_source)