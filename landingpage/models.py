from django.db import models
from datetime import datetime
# Create your models here.

class Status(models.Model):

    message = models.TextField(max_length=300)
    date = models.DateTimeField(default=datetime.now)